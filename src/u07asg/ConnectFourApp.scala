package u07asg

import java.awt._
import java.awt.event.{ActionEvent, ActionListener, MouseAdapter, MouseEvent}
import java.awt.geom.Path2D
import javax.swing._

/**
  * Created by mvenditto.
  */
class ConnectFourGui extends JFrame {

  private[this] val mainPanel: JPanel = new JPanel
  private[this] val topBar: JPanel = new JPanel
  private[this] val playBar: JPanel = new JPanel
  private[this] val btnBar: JPanel = new JPanel
  private[this] val gameBoard: JPanel = new JPanel
  private[this] val winnerLabel: JLabel = new JLabel("")
  private[this] var gameBoardCells: Seq[CircleCell] = scala.collection.immutable.Seq.empty[CircleCell]
  private[this] val connectFour = new ConnectFourImpl
  private[this] var gameStarted = false

  init()

  private def updateBoardView(i: Int) = {
    val board = connectFour.getBoard
    val col = board(i)
    for (j <- 0 to 5) {
      val cell = gameBoardCells(j * 7 + i)
      val owner = col(j) match {
        case "red" => Some(Red)
        case "yellow" => Some(Yellow)
        case _ => None
      }
      cell.setOwnerPlayer(owner)
      gameBoardCells = gameBoardCells.updated(j * 7 + i, cell)
      cell.repaint()
    }
  }

  private def playerMove(i: Int): Unit = {
    if (gameStarted) {
      connectFour.doMove(i)
      updateBoardView(i)
      val possibleWinner = connectFour.checkVictory
      val boardFull = connectFour.isBoardFull
      val winLabelText: String = possibleWinner match {
        case Some(Red) => "Red won!"
        case Some(Yellow) => "Yellow won!"
        case None => if(boardFull) "Draw..." else ""
      }
      if (possibleWinner.isDefined || boardFull) {
        topBar.removeAll()
        topBar.add(btnBar)
        topBar.validate()
        topBar.repaint()
        winnerLabel.setText(winLabelText)
      }
    }
  }

  private def resetGameBoard() = {
    gameBoardCells.foreach((c: CircleCell) => {c.setOwnerPlayer(None); c.repaint()})
  }

  private def init() = {
    mainPanel.setLayout(new BorderLayout(5, 5))
    mainPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5))
    add(mainPanel)
    winnerLabel.setForeground(Color.BLUE)
    val btnRestart = new JButton("Restart")
    val btnExit = new JButton("Exit")
    btnRestart.addActionListener(new ActionListener {
      override def actionPerformed(actionEvent: ActionEvent): Unit = {
        topBar.removeAll()
        topBar.add(playBar)
        topBar.invalidate()
        topBar.repaint()
        connectFour.reset()
        resetGameBoard()
      }
    })
    btnExit.addActionListener(new ActionListener {
      override def actionPerformed(actionEvent: ActionEvent): Unit = System.exit(0)
    })
    btnBar.setLayout(new FlowLayout)
    btnBar.add(winnerLabel)
    btnBar.add(btnRestart)
    btnBar.add(btnExit)
    playBar.setLayout(new GridLayout(1, 7, 4, 4))
    for (i <- 0 to 6) {
      val btn = new PlayTokenButton
      btn.addActionListener(new ActionListener {
        override def actionPerformed(actionEvent: ActionEvent): Unit = playerMove(i)
      })
      playBar.add(btn)
    }
    topBar.setLayout(new BorderLayout(0, 0))
    topBar.add(playBar, BorderLayout.CENTER)
    topBar.setPreferredSize(new Dimension(390, 44))
    mainPanel.add(topBar, BorderLayout.NORTH)
    gameBoard.setLayout(new GridLayout(6, 7, 4, 4))
    gameBoard.setBackground(Color.BLUE)
    for (_ <- 1 to 42) {
      val cell = new CircleCell
      gameBoardCells :+= cell
      gameBoard.add(cell)
    }
    mainPanel.add(gameBoard, BorderLayout.CENTER)
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    this.setSize(400, 350)
    this.setResizable(false)
    this.setVisible(true)
    gameStarted = true
  }

  private class PlayTokenButton extends JButton {

    private[this] var mouseOver = false
    this.setPreferredSize(new Dimension(40, 40))

    this.addMouseListener(new MouseAdapter {
      override def mouseExited(mouseEvent: MouseEvent): Unit = {mouseOver = false; repaint()}
      override def mouseEntered(mouseEvent: MouseEvent): Unit = {mouseOver = true; repaint()}
    })

    private def triangleShape: Path2D.Double = {
      val triangle = new Path2D.Double
      triangle.moveTo(10, (getHeight / 2) - 5)
      triangle.lineTo(getWidth - 10, (getHeight / 2) - 5)
      triangle.lineTo(getWidth / 2, 32)
      triangle.closePath()
      triangle
    }

    override def paintComponent(g: Graphics): Unit = {
      super.paintComponent(g)
      val g2 = g.asInstanceOf[Graphics2D]
      if (mouseOver) {
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
          RenderingHints.VALUE_ANTIALIAS_ON)
        val color = connectFour.getPlayerTurn match {
          case Red => Color.RED
          case Yellow => Color.YELLOW
          case _ => Color.white
        }
        g2.setColor(color)
        g2.fill(triangleShape)
        g2.setColor(Color.DARK_GRAY)
        g2.draw(triangleShape)
      }
    }
  }

  private class CircleCell extends JPanel {

    private[this] var ownerPlayer: Option[Player] = None

    def setOwnerPlayer(player: Option[Player]): Unit = {ownerPlayer = player}
    def getOwnerPlayer: Option[Player] = ownerPlayer

    override def paintComponent(g: Graphics): Unit = {
      super.paintComponent(g)
      val g2 = g.asInstanceOf[Graphics2D]
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON)
      val color = ownerPlayer match {
        case Some(Red) => Color.RED
        case Some(Yellow) => Color.YELLOW
        case _ => Color.WHITE
      }
      g2.setColor(Color.BLUE)
      g2.fillRect(0, 0, g.getClipBounds.width, g.getClipBounds.height)
      g2.setColor(color)
      g2.fillOval(0, 0, g.getClipBounds().width, g.getClipBounds().height)
      g2.setColor(Color.DARK_GRAY)
      g2.setStroke(new BasicStroke(2))
      g2.drawOval(0, 0, g.getClipBounds().width, g.getClipBounds().height)
    }
  }
}

object ConnectFourApp extends App {
  val gui = new ConnectFourGui
}

