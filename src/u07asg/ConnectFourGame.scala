package u07asg

import java.io.FileInputStream

import u07asg.Scala2P._

import alice.tuprolog.{Struct, Theory}

import scala.collection.JavaConverters._

/**
  * Created by mvenditto.
  */

sealed trait Player
object Red extends Player
object Yellow extends Player

sealed trait ConnectFour {
  def getBoard: List[List[String]]

  def getPlayerTurn: Player

  def doMove(column: Int): Boolean

  def checkVictory: Option[Player]

  def isBoardFull: Boolean

  def reset(): Unit
}

class ConnectFourImpl extends ConnectFour {
  private[this] val engine = mkPrologEngine(new Theory(new FileInputStream("src/u07asg/c4.pl")))
  private[this] var playerTurn: Player = Red

  createBoard()

  private def createBoard() = {
    val goal = "retractall(board(_)),create_board(B),assert(board(B))"
    solveWithSuccess(engine, goal)
  }

  private def swapTurn() = {
    playerTurn = playerTurn match {
      case Red => Yellow
      case Yellow => Red
    }
  }

  override def reset(): Unit = {createBoard(); playerTurn=Red}

  override def getPlayerTurn: Player = playerTurn

  override def getBoard: List[List[String]] = {
    val term = solveOneAndGetTerm(engine, "board(B)", "B").asInstanceOf[Struct]
    val iterator = term.listIterator()
    iterator.asScala.toList.map(_.asInstanceOf[Struct].listIterator().asScala.toList.map(_.toString))
  }

  override def checkVictory: Option[Player] = {
    val goal = "board(B),final_board(B,red)"
    val goal2 = "board(B),final_board(B, yellow)"
    if (solveWithSuccess(engine, goal)) Option(Red)
    else if (solveWithSuccess(engine, goal2)) Option(Yellow)
    else None
  }

  override def doMove(column: Int): Boolean = {
    val player = playerTurn match  {
      case Red => "red"
      case Yellow => "yellow"
      case _ => "invalid"
    }
    val goal = "board(B), move(B," + column.toString + ","+player+",NB), retractall(board(_)), assert(board(NB))"
    val isMoveAllowed = solveWithSuccess(engine, goal)
    if (isMoveAllowed) swapTurn()
    isMoveAllowed
  }

  override def isBoardFull: Boolean = {
    val goal = "board(B), complete_board(B)"
    solveWithSuccess(engine, goal)
  }
}


object TestC4 extends  App {
  val c4 = new ConnectFourImpl
  val board = c4.getBoard
  println(board)
  println(c4.doMove(0))
  println(c4.doMove(1))
  println(c4.doMove(0))
  println(c4.doMove(1))
  println(c4.doMove(0))
  println(c4.doMove(1))
  println(c4.doMove(0))
  println(c4.doMove(1))
  println(c4.checkVictory)
  println(c4.getBoard)
  println(c4.isBoardFull)
}
