opponent(red, yellow).
opponent(yellow, red).

player(red).
player(yellow).

result(red, 'red wins!').
result(yellow, 'yellow wins!').
result(draw, 'draw!').

create_board(B) :- create_list(6, null, C), create_list(7,C,B).
create_list(0,_,[]) :- !.
create_list(N,X,[X|T]) :- N2 is N-1, create_list(N2,X,T).

%get_at(+List, +Index, -Item) - get the i-th item of the list
get_at([C|Cs], I, T) :- get_at([C|Cs], I, 0, T).
get_at([C|Cs], I, Count, T) :- Count==I, T=C, !.
get_at([C|Cs], I, Count, T) :- Count2 is Count+1, get_at(Cs, I, Count2, T2), T=T2.

%get_row(+Board, +Index, -Row) - given the game board(list of columns) get the i-th row
get_row([B|Bs], I, R) :- get_row([B|Bs], I, [], R2), R=R2.
get_row([], _, T, R) :- R=T.
get_row([B|Bs], I, T, R) :- get_at(B, I, E), append(T, [E], T2), get_row(Bs, I, T2, R).

%add_token(+Column, +Player, -ResultingColumn) - creates the column resulting from the addition of a token to Column
add_token(C, red, R) :- add_tok_patt(C, R, red), !.
add_token(C, yellow, R) :- add_tok_patt(C, R, yellow), !.
add_tok_patt([null,null,null,null,null,null], [null,null,null,null,null,T], T).
add_tok_patt([null,null,null,null,null,A], [null,null,null,null,T,A], T).
add_tok_patt([null,null,null,null,A,B], [null,null,null,T,A,B], T).
add_tok_patt([null,null,null,A,B,C], [null,null,T,A,B,C], T).
add_tok_patt([null,null,A,B,C,D], [null,T,A,B,C,D], T).
add_tok_patt([null,A,B,C,D,E], [T,A,B,C,D,E], T).

%board_update(+Board, +NewColIndex, +NewColumn, -NewBoard) - update a column in the game board
board_update([C|Cs], NCI, NC, NB) :- board_update([C|Cs], NCI, NC, 0, [], NB).
board_update([C|Cs], NCI, NC, I, T, NB) :-  I==NCI, append(T, [NC|Cs], NB), !.
board_update([C|Cs], NCI, NC, I, T, NB) :-
	I2 is I + 1,
	append(T, [C], T2),
	board_update(Cs, NCI, NC, I2, T2, NB).

%move(+Board, +ColIndex, +Player, -NewBoard) - get the resulting board after Player put a token in column at ColIndex
move(B, CI, P, NB) :-
	get_at(B, CI, Col),
	add_token(Col, P, NewCol),
	board_update(B, CI, NewCol, NB).

%four_in_col_patt(?Column, ?Player) - match four token in a Column pattern for Player
four_in_col_patt([T, T, T, T, _, _], T).
four_in_col_patt([_, T, T, T, T, _], T).
four_in_col_patt([_, _, T, T, T, T], T).

%four_in_column(+Board, ?Player) - check if there are four tokens in a column
four_in_column(B, P) :-
	member(Col, B),
	four_in_col_patt(Col, P), player(P), !.

%four_in_col_patt(?Row, ?Player) - match four token in a Row pattern for Player
four_in_row_patt([T, T, T, T, _, _, _], T).
four_in_row_patt([_, T, T, T, T, _, _], T).
four_in_row_patt([_, _, T, T, T, T, _], T).
four_in_row_patt([_, _, _, T, T, T, T], T).

%four_in_column(+Board, ?Player) - check if there are four tokens in a row
four_in_row(B, P) :-
	member(RowIdx, [0,1,2,3,4,5,6]),
	get_row(B, RowIdx, Row),
	four_in_row_patt(Row, P), player(P), !.

%four_in_col_patt(+FourColumnsGroup, ?Player) - check if there are four tokens in diagonal in FourColumnsGroup
four_in_diag_patt([[T|_], [_,T|_], [_,_,T|_], [_,_,_,T|_]], T).
four_in_diag_patt([[_,T|_], [_,_,T|_], [_,_,_,T|_], [_,_,_,_,T|_]], T).
four_in_diag_patt([[_,_,T|_], [_,_,_, T|_], [_,_,_,_,T|_], [_,_,_,_,_,T|_]], T).

%four_in_diagonal(+Board, ?Player) - check if there are four tokens in diagonal on the Board
four_in_diagonal([A, B, C, D, E, F, G], P) :-
	%forward diagonal
	four_in_diag_patt([A, B, C, D], P), player(P), !;
	four_in_diag_patt([B, C, D, E], P), player(P), !;
	four_in_diag_patt([C, D, E, F], P), player(P), !;
	four_in_diag_patt([D, E, F, G], P), player(P), !;
	%backward diagonal
	four_in_diag_patt([D, C, B, A], P), player(P), !;
	four_in_diag_patt([E, D, C, B], P), player(P), !;
	four_in_diag_patt([F, E, D, C], P), player(P), !;
	four_in_diag_patt([G, F, E, D], P), player(P).

%complete_board(+Board) - check if board is full
complete_board(B) :- forall(member(C, B), not(member(null, C))).

%final_board(+Board, ?WinningPlayer) - check if Board is final for a player
final_board(B, WP) :- four_in_column(B, WP), !.
final_board(B, WP) :- four_in_row(B, WP), !.
final_board(B, WP) :- four_in_diagonal(B, WP), !.
final_board(B, WP) :- complete_board(B), WP=draw.


test(R) :- R = [
[red, red, red, yellow, red, red],
[null, red, null, null, null, null],
[null, null, red, null, null, null],
[null, null, null, red, null, null],
[null, null, null, null, null, null],
[null, null, null, null, null, null],
[null, null, null, null, null, null]].
